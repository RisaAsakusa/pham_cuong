    <?php get_header(); ?>
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <!-- Blog Entries Column -->
        <div class="col-md-8">
          <h1 class="my-4">Page Heading
            <small>Secondary Text</small>
          </h1>
         <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="card mb-4">
                <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
                <div class="card-body">
                    <h2 class="card-title"><?php the_title(); ?></h2>
                    <?php the_content('<div style="color: green">Read more ...</div>'); ?>
                </div><!--end post header-->
            </div>
                <div class="entry clear">
                  <?php edit_post_link(); ?>
                  <?php wp_link_pages(); ?> 
                </div>
                <!--end entry-->
                <div class="post-footer">
                  <div class="comments"><?php comments_popup_link( 'Leave a Comment', '1 Comment', '% Comments' ); ?></div>
                </div><!--end post footer-->
              </div><!--end post-->
            <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
                <div class="navigation index">
                <div class="alignleft"><?php next_posts_link( 'Older Entries' ); ?></div>
                <div class="alignright"><?php previous_posts_link( 'Newer Entries' ); ?></div>
                </div><!--end navigation-->
            <?php else : ?>
            <?php endif; ?>
          <!-- Pagination -->
          <ul class="pagination justify-content-center mb-4">
            <li class="page-item">
              <a class="page-link" href="#">&larr; Older</a>
            </li>
            <li class="page-item disabled">
              <a class="page-link" href="#">Newer &rarr;</a>
            </li>
          </ul>

        </div>
        <?php get_sidebar(); ?>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
<?php get_footer(); ?>