<?php 
function reg_scripts() {
        wp_enqueue_style( 'bootstrapthemestyle', get_template_directory_uri() . '/vendor/bootstrap/css/bootstrap.css', array(), '1.0.0', 'all' );
        wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/vendor/jquery/jquery.min.js',
        array(), true);
  }
  add_action( 'wp_enqueue_scripts', 'reg_scripts' );
?>