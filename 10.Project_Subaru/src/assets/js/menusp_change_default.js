function myFunction2(x) {
    if (x.matches) {
        $(document).ready(function () {
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();
                if (scroll > 50) {
                    $(".c-menu-sp").css("background", "#fff");
                    $(".c-menu-sp").css("box-shadow", "rgba(136, 136, 136, 0.59) 0px 7px 20px");
                }
                else {
                    $(".c-menu-sp").css("background", "none");
                    $(".c-menu-sp").css("box-shadow", "none");
                }
            })
        })
    }
}
var x = window.matchMedia("(max-width: 768px)") // get value x with media have max-width is 1000px
myFunction2(x) // Call listener function at run time
x.addListener(myFunction2) // Attach listener function on state changes