//-------------------For website-----------------
function myFunction2(x) {
    if (!x.matches) {
        $(document).ready(function () {
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();
                if (scroll > 100) {
                    $(".c-menu").css("background", "#fff");
                    $(".c-menu").css("box-shadow", "rgba(136, 136, 136, 0.59) 0px 7px 20px");
                    $(".c-menu .u-logo img").attr("src","assets/img/dex-h-logosm.png")
                }

                else {
                    $(".c-menu").css("background", "none");
                    $(".c-menu").css("box-shadow", "none");
                    $(".c-menu .u-logo img").attr("src","assets/img/dex-h-logosm.png")
                }
            })
        })
    }
//-------------------For Mobile-----------------
    else {
        $(document).ready(function () {
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();
                if (scroll > 100) {
                    $(".c-menu").css("background", "#49c8b2");
                    $(".c-menuSP__1").css("background","#FFF");
                    $(".c-menuSP__2").css("background","#FFF");
                    $(".c-menuSP__3").css("background","#FFF")
                    $(".c-menu").css("box-shadow", "rgba(136, 136, 136, 0.59) 0px 7px 20px");
                    $(".c-menu .u-logo img").attr("src","assets/img/dex-h-logosm-mob-w.png")
                    $(".c-menu .u-plugin .facejs img").attr("src","assets/img/dex-h-face-mob-w.png")
                    $(".c-menu .u-plugin .twijs img").attr("src","assets/img/dex-h-twiter-mob-w.png")
                }
        
                else {
                    $(".c-menu").css("background", "none");
                    $(".c-menuSP__1").css("background","#000");
                    $(".c-menuSP__2").css("background","#000");
                    $(".c-menuSP__3").css("background","#000")
                    $(".c-menu").css("box-shadow", "none");
                    $(".c-menu .u-logo img").attr("src","assets/img/dex-h-logosm-mob.png")
                    $(".c-menu .u-plugin .facejs img").attr("src","assets/img/dex-h-face-mob.png")
                    $(".c-menu .u-plugin .twijs img").attr("src","assets/img/dex-h-twiter.png")
                }
            })
        })        
    }
}
var x = window.matchMedia("(max-width: 768px)") // get value x with media have max-width is 1000px
myFunction2(x) // Call listener function at run time
x.addListener(myFunction2) // Attach listener function on state changes